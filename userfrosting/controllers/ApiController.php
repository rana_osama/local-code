<?php

namespace UserFrosting;

use \Illuminate\Database\Capsule\Manager as Capsule;

/**
 * ApiController Class
 *
 * Controller class for /api/* URLs.  Handles all api requests.
 *
 * @package UserFrosting
 * @author Alex Weissman
 * @link http://www.userfrosting.com/navigating/#structure
 */
class ApiController extends \UserFrosting\BaseController {

    /**
     * Create a new ApiController object.
     *
     * @param UserFrosting $app The main UserFrosting app.
     */
    public function __construct($app){
        $this->_app = $app;
    }
    
	/*
	For Getting user reward points
	*/
	public function getRewardPoint()
	{
		$get = $this->_app->request->get();
		if(isset($get['id']))
		{
			$points = DsConsumer::where("uf_userid",$get['id'])->first(array("reward_points"));
			if($points)
			{
				echo(json_encode($points->toarray()));
			}
			else
			{
				echo "No Data Found";
			}
			
		}
		else
		{
			echo("Invalid Info");
		}
		
	}
	
	/*
	For Getting user's currently selected reward
	*/
	public function getReward()
	{
		$get = $this->_app->request->get();
		if(isset($get['id']))
		{
			$points = DsConsumer::where("uf_userid",$get['id'])->first(array("selected_reward"));
			if($points)
			{
				$points = $points->toarray();
				$reward = DsReward::where("id", $points["selected_reward"])->get(array("id", "name", "pic_ref", "description", "points", "stock"));
				$rewards['info'] = $reward->toarray();
				$data = array();
				array_push($data, $rewards);
				echo(json_encode($data));
			}
			else
			{
				echo "No Data Found";
			}
			
		}
		else
		{
			echo("Invalid Info");
		}
		
	}

	/*
	For Getting product info
	*/
	public function getProductInfo()
	{
		$get = $this->_app->request->get();
		if(isset($get['id']))
		{
			$product = DsProduct::where("id",$get['id'])->first();
			if($product)
			{
				$product = $product->toarray();
				$data = array();
				array_push($data, $product);
				echo(json_encode($data));
			}
			else
			{
				echo "No Data Found";
			}
			
		}
		else
		{
			echo("Invalid Info");
		}
		
	}

	/*
	For Getting kick info
	*/
	public function getKickInfo()
	{
		$get = $this->_app->request->get();
		if(isset($get['id']))
		{
			$kick = DsKick::where("id",$get['id'])->first();
			if($kick)
			{
				$kick = $kick->toarray();
				$data = array();
				array_push($data, $kick);
				echo(json_encode($data));
			}
			else
			{
				echo "No Data Found";
			}
			
		}
		else
		{
			echo("Invalid Info");
		}
		
	}
	
	/*
	For getting information of  store  
	*/
	public function storeInfo()
	{
		$get = $this->_app->request->get();
		if(isset($get['id']))
		{
			$store = DsRetailer::where("id",$get['id'])->first();
			if($store)
			{
				$data['store_info'] = [
				"id" => $store['id'],
				"name" => $store['retailer_name'],
				"logo" => $store['logo'],
				"banner" => null,//$store[''],
				"address" => $store['address'],
				"coordinates" => $store['gps_coordinate'],
				"description" => null,//$store[''],
				"offer" => null,//$store[''],
				"product" => null,//$store[''],
				"kick" => null,//$store['']
					];
				$stores = array();
				array_push($stores, $data);
				echo(json_encode($stores));
			}
			else
			{
				echo "No Data Found";
			}
		}
		else
		{
			echo("Invalid Info");
		}
	}
	
	/*
	For getting information of  users  
	*/
	
	public function userLogin()
	{
		$get = $this->_app->request->get();
		if(isset($get['email']) && isset($get['pass']))
		{
			$user = User::where("email",$get['email'])->first(array("id", "display_name", "password"));
			if($user)
			{
				$pass = substr($get['pass'],5,-5);
				if($user->verifyPassword($pass))  {
					$user = $user->toarray();
					$consumer = DsConsumer::where("uf_userid", $user['id'])->first(array("dob", "reward_points", "selected_reward"));
					if($consumer)
					{
						$consumer = $consumer->toarray();
					}
					else
					{
						echo("This is not an end user");
						break;
					}
					$coupons = DsConsumerCoupon::where("consumer_id",$user['id'])->get(array("coupon_id"));
					if($coupons)
					{
						$coupons = $coupons->toarray();
					}
					else
					{
					}
					$data['userinfo'] = [
						"id" => $user['id'],
						"display_name" => $user['id'],
						"dob" => $consumer['dob'],
						"picture" => null,
						"age" => date_diff(date_create($consumer['dob']), date_create('today'))->y,
						"reward_points" => $consumer['reward_points'],
						"coupons" => $coupons,
						"token" => md5($user['id']." ".date("Y-m-d H:i:s")),
						"selected_reward" => $consumer['selected_reward']
					];
					$login["login"] = "true";
					$data1 = array();
					array_push($data1, $login);
					array_push($data1, $data);
					echo(json_encode($data1));
				}
				else
				{
					echo ("false");
				}
			}
			else
			{
				echo ("false");
			}
		}
		else
		{
			echo("false");
		}
	}
	/*
	To Fetch List of Stores
	*/
	public function getList()
	{
		$get = $this->_app->request->get();
		if(isset($get['list']))
		{
			if($get['list'] == "retailer")
			{
				if(isset($get['page']) && $get['page'] != '1')
				{
					$ofset = $get['page'] * 50;
					$list= DsRetailer::take(50)->skip($ofset)->get(array('id', 'retailer_name'));
				}
				else
				{
					$list= DsRetailer::take(50)->get(array('id', 'retailer_name'));
				}
				echo(json_encode($list->toarray()));
			}
			elseif($get['list'] == "product")
			{
				if(isset($get['page']) && $get['page'] != '1')
				{
					$ofset = $get['page'] * 50;
					$list= DsProduct::take(50)->skip($ofset)->get(array('id', 'name'));
				}
				else
				{
					$list= DsProduct::take(50)->get(array('id', 'name'));
				}
				echo(json_encode($list->toarray()));
			}
			elseif($get['list'] == "kick")
			{
				if(isset($get['page']) && $get['page'] != '1')
				{
					$ofset = $get['page'] * 50;
					$list= DsKick::take(50)->skip($ofset)->get(array('id', 'text'));
				}
				else
				{
					$list= DsKick::take(50)->get(array('id', 'text'));
				}
				echo(json_encode($list->toarray()));
			}
			else
			{
				echo(json_encode("No Data found"));
			}
		}
		else
		{
			echo(json_encode("No data found"));
		}
	}
	
	public function redeamCoupon()
	{
		$get = $this->_app->request->get();
		if(isset($get['userid']) && isset($get['couponid']))
		{
			$coupon = DsConsumerCoupon::where("consumer_id", $get['userid'])->where("coupon_id", $get['couponid'])->get();
			if(sizeof($coupon) > 0)
			{
				$id=$coupon->toarray();
				//echo "working";exit;
				$data = [
					"consumer_id" => $id[0]['consumer_id'],
					"coupon_id" => $id[0]['coupon_id'],
					"redeamption_date" => date("Y-m-d H:i:s")
					];
					//print_r($data);exit;
				$addcoupon = new DsConsumerCouponHistory($data);
				$addcoupon->save();
				$coupon = DsConsumerCoupon::find($id[0]['id']);
				$coupon->delete();
				echo "success";
			}
			else
			{
				echo "No data found";
			}
		}
	}
	
	public function getCoupon()
	{
		$get = $this->_app->request->get();
		if(isset($get['userid']) && isset($get['retid']))
		{
			//$list = DsCoupon::where("retailer_id", $get['retid'])->get();
			//$userid = DsConsumer::where("card_ref",$get['userid'])->get(array("uf_userid"));
			//$userid = $userid->toarray();
			$list = DsConsumerCoupon::where("consumer_id", $get['userid'])->get(array("coupon_id"));
			$copounids=array();
			$list = $list->toarray();
			$coupons = DsCoupon::where("retailer_id", $get['retid'])->get(array("id","text","qr_code","bar_code","pic","expiry"));
			$coupons = $coupons->toarray();
			$finalcoupons = array();
			//echo"working";exit;
			foreach($coupons as $coupon)
			{
				for($i=0;$i < sizeof($list); $i++)
				{
					if($coupon['id'] == $list[$i]['coupon_id'])
					{
						array_push($finalcoupons,$coupon);
					}
				}
			}
			echo(json_encode($finalcoupons));
			
		}
		else
		{
			echo("Invalid data");
		}
	}
	
	public function rewardStatus(){
		//echo (json_encode('Still working'));
		$get = $this->_app->request->get();
		$status = $get['reward'];
		$ID = $get['Id'];
		//$reward = DsReward::where("id", $ID)->get();
		$reward = DsReward::find($ID);
		$reward->status = $status;
		$reward->save();
		
	}
	
	public function getCouponBarcode()
	{
		$get = $this->_app->request->get();
		if(isset($get['bar']))
		{
			$copoun = DsCoupon::where("bar_code", $get['bar'])->get();
			$copounids=array();
			$copoun = $copoun->toarray();
			if(sizeof($copoun) > 0)
			{
				echo(json_encode($copoun));
			}
			else
			{
				echo("No Data Found");
			}
			
			
		}
		else
		{
			echo("Invalid data");
		}
	}
	
	public function getUser()
	{
		$post = $this->_app->request->get();
		if(isset($post['email'])&& strlen($post['email']) > 0)
		{
			$user= User::where("email",$post['email'])->first();
			$consumer = DsConsumer::where("uf_userid", $user['id'])->first();
		}
		elseif(isset($post['phone'])&& strlen($post['phone']) > 0)
		{
			$consumer = DsConsumer::where("contact", $post['phone'])->first();
			$user= User::where("id",$consumer['uf_userid'])->first();
		}
		elseif(isset($post['dob'])&& strlen($post['dob']) > 0)
		{
			$consumer = DsConsumer::where("dob", $post['dob'])->first();
			$user= User::where("id",$consumer['uf_userid'])->first();
			
		}
		$data1= array();
		$data=[
			"id" => $user['id'],
			"name"=>$user['display_name'],
			"dob"=>$consumer['dob'],
			"email"=>$user['email'],
			"phone"=>$consumer['contact']
			];
		array_push($data1,$data);
		if($data['name'] == null && $data['dob'] == null && $data['email'] == null && $data['phone'] == null)
		{
			echo json_encode("no data found");
		}
		else
		{
			echo(json_encode($data1));
		}
	}
	
	public function createCoupon()
	{
		$get = $this->_app->request->get();
		$data =[
			"text" => $get['text'],
			"bar_code" => $get['bar_code'],
			"pic" => $get['pic'],
			"expiry" => $get['expiry'],
			"retailer_id" => $get['ret_id'],
			"valid" => 1
			];
		$coupon = new DsCoupon($data);
		$coupon->save();
		
		$newcoupon = DsCoupon::where("retailer_id",$get['ret_id'])->where("text", $get['text'])->first(array("id"));
		$newcoupon = $newcoupon->toarray();
		//$newcoupon = $newcoupon['id'];
		
		$data1 = [
			"coupon_id" => $newcoupon['id'],
			"consumer_id" => 4
			];
		$consumer_coupons = new DsConsumerCoupon($data1);
		$consumer_coupons->save();
		echo 'Success';
	}
	
	public function getCardInfo()
	{
		$get = $this->_app->request->get();
		$consumer = DsConsumer::where("card_ref", $get['cardid'])->first(array("uf_userid","contact","dob"));
		$consumer = $consumer->toarray();
		//echo($consumer['contact']);exit;
		$user = User::where("id", $consumer['uf_userid'])->first(array("display_name", "email"));
		$user = $user->toarray();
		$data = [
			"id" => $consumer['uf_userid'],
			"name" => $user['display_name'],
			"dob" => $consumer['dob'],
			"phone" => $consumer['contact'],
			"email" => $user['email']
			];
		$data1 = array();
		array_push($data1, $data);
		echo(json_encode($data1));
    /**
     * Returns a list of Users
     *
     * Generates a list of users, optionally paginated, sorted and/or filtered.
     * This page requires authentication.
     * Request type: GET
     */        
	}
    public function listUsers(){
        $get = $this->_app->request->get();
                
        $size = isset($get['size']) ? $get['size'] : null;
        $page = isset($get['page']) ? $get['page'] : null;
        $sort_field = isset($get['sort_field']) ? $get['sort_field'] : "user_name";
        $sort_order = isset($get['sort_order']) ? $get['sort_order'] : "asc";
        $filters = isset($get['filters']) ? $get['filters'] : [];
        $format = isset($get['format']) ? $get['format'] : "json";
        $primary_group_name = isset($get['primary_group']) ? $get['primary_group'] : null;  
        
        // Optional filtering by primary group
        if ($primary_group_name){
            $primary_group = Group::where('name', $primary_group_name)->first();
            
            if (!$primary_group)
                $this->_app->notFound();
            
            // Access-controlled page
            if (!$this->_app->user->checkAccess('uri_group_users', ['primary_group_id' => $primary_group->id])){
                $this->_app->notFound();
            }
            
            $userQuery = new User;
            $userQuery = $userQuery->where('primary_group_id', $primary_group->id);
            
        } else {
            // Access-controlled page
            if (!$this->_app->user->checkAccess('uri_users')){
                $this->_app->notFound();
            }
            
            $userQuery = new User;
        }
                
        // Count unpaginated total
        $total = $userQuery->count();
            
        // Exclude fields
        $userQuery = $userQuery
                ->exclude(['password', 'secret_token']);
        
        //Capsule::connection()->enableQueryLog();
        
        // Get unfiltered, unsorted, unpaginated collection
        $user_collection = $userQuery->get();
        
        // Load recent events for all users and merge into the collection.  This can't be done in one query,
        // at least not efficiently.  See http://laravel.io/forum/04-05-2014-eloquent-eager-loading-to-limit-for-each-post
        $last_sign_in_times = $user_collection->getRecentEvents('sign_in');
        $last_sign_up_times = $user_collection->getRecentEvents('sign_up', 'sign_up_time');
        
        // Apply filters        
        foreach ($filters as $name => $value){
            // For date filters, search for weekday, month, or year
            if ($name == 'last_sign_in_time') {
                $user_collection = $user_collection->filterRecentEventTime('sign_in', $last_sign_in_times, $value);
            } else if ($name == 'sign_up_time') {
                $user_collection = $user_collection->filterRecentEventTime('sign_up', $last_sign_up_times, $value, "Unknown");
            } else {
                $user_collection = $user_collection->filterTextField($name, $value);
            }
        }
        
        // Count filtered results
        $total_filtered = count($user_collection);
        
        // Sort
        if ($sort_order == "desc")
            $user_collection = $user_collection->sortByDesc($sort_field, SORT_NATURAL|SORT_FLAG_CASE);
        else        
            $user_collection = $user_collection->sortBy($sort_field, SORT_NATURAL|SORT_FLAG_CASE);
        
        // Paginate
        if ( ($page !== null) && ($size !== null) ){
            $offset = $size*$page;
            $user_collection = $user_collection->slice($offset, $size);
        }
        
        $result = [
            "count" => $total,
            "rows" => $user_collection->values()->toArray(),
            "count_filtered" => $total_filtered
        ];
        
        //$query = Capsule::getQueryLog();    
        
        if ($format == "csv"){
            $settings = http_build_query($get);
            $date = date("Ymd");
            $this->_app->response->headers->set('Content-Disposition', "attachment;filename=$date-users-$settings.csv");
            $this->_app->response->headers->set('Content-Type', 'text/csv; charset=utf-8');
            $keys = $user_collection->keys()->toArray();
            echo implode(array_keys($result['rows'][0]), ",") . "\r\n";
            foreach ($result['rows'] as $row){
                echo implode($row, ",") . "\r\n";    
            }   
        } else {
            // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
            // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
            $this->_app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
            echo json_encode($result, JSON_PRETTY_PRINT);
        }
    }
}
