/*
SQLyog Enterprise - MySQL GUI v8.14 
MySQL - 5.6.17 : Database - userfrosting
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`userfrosting` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `userfrosting`;

/*Table structure for table `authorize_group` */

DROP TABLE IF EXISTS `authorize_group`;

CREATE TABLE `authorize_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `hook` varchar(200) NOT NULL COMMENT 'A code that references a specific action or URI that the group has access to.',
  `conditions` text NOT NULL COMMENT 'The conditions under which members of this group have access to this hook.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `authorize_group` */

insert  into `authorize_group`(`id`,`group_id`,`hook`,`conditions`) values (1,1,'uri_dashboard','always()'),(2,2,'uri_dashboard','always()'),(3,2,'uri_users','always()'),(4,1,'uri_account_settings','always()'),(5,1,'update_account_setting','equals(self.id, user.id)&&in(property,[\"email\",\"locale\",\"password\"])'),(6,2,'update_account_setting','!in_group(user.id,2)&&in(property,[\"email\",\"display_name\",\"title\",\"locale\",\"flag_password_reset\",\"flag_enabled\"])'),(7,2,'view_account_setting','in(property,[\"user_name\",\"email\",\"display_name\",\"title\",\"locale\",\"flag_enabled\",\"groups\",\"primary_group_id\"])'),(8,2,'delete_account','!in_group(user.id,2)'),(9,2,'create_account','always()'),(10,4,'uri_consumers','always()'),(11,5,'uri_reward','always()'),(12,3,'create_account','always()'),(13,6,'create_coupon','always()'),(14,6,'uri_coupon','always()\r\n');

/*Table structure for table `authorize_user` */

DROP TABLE IF EXISTS `authorize_user`;

CREATE TABLE `authorize_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `hook` varchar(200) NOT NULL COMMENT 'A code that references a specific action or URI that the user has access to.',
  `conditions` text NOT NULL COMMENT 'The conditions under which the user has access to this action.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `authorize_user` */

/*Table structure for table `configuration` */

DROP TABLE IF EXISTS `configuration`;

CREATE TABLE `configuration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plugin` varchar(50) NOT NULL COMMENT 'The name of the plugin that manages this setting (set to ''userfrosting'' for core settings)',
  `name` varchar(150) NOT NULL COMMENT 'The name of the setting.',
  `value` longtext NOT NULL COMMENT 'The current value of the setting.',
  `description` text NOT NULL COMMENT 'A brief description of this setting.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='A configuration table, mapping global configuration options to their values.';

/*Data for the table `configuration` */

insert  into `configuration`(`id`,`plugin`,`name`,`value`,`description`) values (1,'userfrosting','site_title','DealSmash','The title of the site.  By default, displayed in the title tag, as well as the upper left corner of every user page.'),(2,'userfrosting','admin_email','admin@dealsmash.com','The administrative email for the site.  Automated emails, such as verification emails and password reset links, will come from this address.'),(3,'userfrosting','email_login','1','Specify whether users can login via email address or username instead of just username.'),(4,'userfrosting','can_register','0','Specify whether public registration of new accounts is enabled.  Enable if you have a service that users can sign up for, disable if you only want accounts to be created by you or an admin.'),(5,'userfrosting','enable_captcha','0','Specify whether new users must complete a captcha code when registering for an account.'),(6,'userfrosting','require_activation','1','Specify whether email verification is required for newly registered accounts.  Accounts created by another user never need to be verified.'),(7,'userfrosting','resend_activation_threshold','0','The time, in seconds, that a user must wait before requesting that the account verification email be resent.'),(8,'userfrosting','reset_password_timeout','10800','The time, in seconds, before a user\'s password reset token expires.'),(9,'userfrosting','create_password_expiration','86400','The time, in seconds, before a new user\'s password creation token expires.'),(10,'userfrosting','default_locale','en_US','The default language for newly registered users.'),(11,'userfrosting','guest_theme','default','The template theme to use for unauthenticated (guest) users.'),(12,'userfrosting','minify_css','0','Specify whether to use concatenated, minified CSS (production) or raw CSS includes (dev).'),(13,'userfrosting','minify_js','0','Specify whether to use concatenated, minified JS (production) or raw JS includes (dev).'),(14,'userfrosting','version','0.3.1.10','The current version of UserFrosting.'),(15,'userfrosting','author','Xecofy','The author of the site.  Will be used in the site\'s author meta tag.'),(16,'userfrosting','show_terms_on_register','0','Specify whether or not to show terms and conditions when registering.'),(17,'userfrosting','site_location','Pakistan','The nation or state in which legal jurisdiction for this site falls.'),(18,'userfrosting','install_status','complete',''),(19,'userfrosting','root_account_config_token','','');

/*Table structure for table `ds_consumer` */

DROP TABLE IF EXISTS `ds_consumer`;

CREATE TABLE `ds_consumer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uf_userid` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` int(1) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` text,
  `contact` varchar(15) DEFAULT NULL,
  `delivery_prefference` int(1) DEFAULT NULL,
  `notifications` int(1) NOT NULL DEFAULT '1',
  `card_ref` varchar(100) DEFAULT NULL,
  `question` int(11) NOT NULL,
  `answere` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `ds_consumer` */

insert  into `ds_consumer`(`id`,`uf_userid`,`first_name`,`last_name`,`gender`,`dob`,`address`,`contact`,`delivery_prefference`,`notifications`,`card_ref`,`question`,`answere`) values (1,2,'Osama','Rana',1,'1992-09-28','R-1519/18, F.B, Karachi','121320326',2,1,NULL,0,''),(3,4,'Kashan','Test',1,'2010-03-15','skbcks1121','5123567',3,1,NULL,2147483647,''),(4,3,'rana','osama',1,'2005-05-12','15ab, D-15, Islamabad','1233545',4,0,NULL,429280426,''),(6,7,'Sami','ullah',1,'1982-01-01','Bahria town, Islamabad, Pakistan','0321554826',4,1,NULL,0,''),(9,10,'Hamood','Riasat Ali',1,'1982-08-28','riadh, KSA','035468226',3,1,NULL,2147483647,''),(10,11,'Ali','Husnain',2,'1980-09-28','Nobility','03546594321',3,0,NULL,0,''),(11,13,'Ali','Husnain Shah',1,'1978-05-16','Rawalpindi, Pakistan','5254564269',3,1,NULL,2147483647,''),(16,20,'Test','For Question',2,'1985-05-23','Testing 2132aff','24632165',3,0,'',3,'answere3');

/*Table structure for table `ds_consumer_coupon_history` */

DROP TABLE IF EXISTS `ds_consumer_coupon_history`;

CREATE TABLE `ds_consumer_coupon_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `redeamption_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `ds_consumer_coupon_history` */

insert  into `ds_consumer_coupon_history`(`id`,`consumer_id`,`coupon_id`,`redeamption_date`) values (1,2,4,'2015-12-31 08:25:25'),(2,4,15,'2016-01-04 06:44:24'),(3,4,15,'2016-01-04 06:49:04'),(4,4,22,'2016-01-04 07:44:06'),(5,4,9,'2016-01-04 09:07:45');

/*Table structure for table `ds_consumer_valid_coupons` */

DROP TABLE IF EXISTS `ds_consumer_valid_coupons`;

CREATE TABLE `ds_consumer_valid_coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `ds_consumer_valid_coupons` */

insert  into `ds_consumer_valid_coupons`(`id`,`coupon_id`,`consumer_id`) values (3,1,1),(4,5,1),(5,6,1),(6,3,1),(7,14,1),(8,8,4),(10,10,4),(13,17,4),(14,18,4),(15,19,4),(16,20,4),(17,21,4);

/*Table structure for table `ds_coupons` */

DROP TABLE IF EXISTS `ds_coupons`;

CREATE TABLE `ds_coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(1000) NOT NULL,
  `qr_code` varchar(50) DEFAULT NULL,
  `bar_code` varchar(50) DEFAULT NULL,
  `pic` varchar(500) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `retailer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `ds_coupons` */

insert  into `ds_coupons`(`id`,`text`,`qr_code`,`bar_code`,`pic`,`expiry`,`retailer_id`) values (1,'New Coupon',NULL,NULL,NULL,NULL,2),(2,'My new coupon',NULL,NULL,NULL,NULL,3),(3,'new coupon',NULL,NULL,NULL,NULL,3),(4,'Get Discount',NULL,NULL,NULL,NULL,3),(5,'Sale',NULL,NULL,NULL,NULL,2),(6,'Half Price',NULL,NULL,NULL,NULL,2),(8,'50% off at Ethnic',NULL,'123456789','http://thumbs.dreamstime.com/z/ethnic-sun-12380940.jpg','2016-01-10',1),(9,'Buy one get one free',NULL,'012000014338','https://upload.wikimedia.org/wikipedia/en/d/da/Khaadi_logo.png','2016-01-16',1),(10,'Khakis 15% off',NULL,'789456123','https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRf0pEHiMEalOp6GOVp0VM01ML5ASwNVXiKOoGbfZX8x-TAbaMVyQ','2016-01-19',1),(14,'My new Coupon',NULL,'126562359','https://ubuntualways.files.wordpress.com/2011/08/google-chrome-logo-wallpaper.jpg','2016-09-28',1),(15,'dirty coupon',NULL,'555555555','no pic ','2016-01-19',1),(16,'dirty coupon',NULL,'555555555','no pic ','2016-01-19',1),(17,'Shirts free with two pants',NULL,'321456987','http://www.thenewstrack.com/~hotlink-cache/wp-content/uploads/2014/10/Riverstone-Western-Casual-Dresses-Collection-For-Boys-Girls-2014-2015-10.jpg','2016-01-19',1),(18,'Garments cleaning products',NULL,'569852145','http://www.thenewstrack.com/~hotlink-cache/wp-content/uploads/2014/10/Riverstone-Western-Casual-Dresses-Collection-For-Boys-Girls-2014-2015-10.jpghttp://kaya.pk/image/cache/data/3a/Square%20Corner%20Shelf%20in%20Pakistan-2-800x800.jpg','2016-01-19',1),(19,'Cheap lawn',NULL,'856541235','http://www.thenewstrack.com/~hotlink-cache/wp-content/uploads/2014/10/Riverstone-Western-Casual-Dresses-Collection-For-Boys-Girls-2014-2015-10.jpghttp://kaya.pk/image/cache/data/3a/Square%20Corner%20Shelf%20in%20Pakistan-2-800x800.jpghttps://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTiX9p9Cq1YzsmLkClWWrxhlK9p6HArZa_bMAsT4aVRsQCsYpDF','2016-01-19',1),(20,'20% off at new arrival',NULL,'785412369','http://www.pakreviews.com/sites/default/files/images/20142111170706.jpg','2016-01-20',1),(21,'summer collection 20% off',NULL,'365478521','http://fashion360.pk/wp-content/uploads/2014/04/Bombaywala-Latest-Dresses-Collection-2014-For-Ladies-3-200x200.jpg','2016-01-31',1),(22,'Skinny trousers',NULL,'985645721','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSs5mS01_bWMWKkPXQ705Ff0D_MD-uXMfSVoDtWGb6YWomCD0ie','2016-02-01',1);

/*Table structure for table `ds_coupons1` */

DROP TABLE IF EXISTS `ds_coupons1`;

CREATE TABLE `ds_coupons1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(100) NOT NULL,
  `discount` varchar(2) NOT NULL,
  `details` text,
  `expiry` date DEFAULT NULL,
  `bar_code` varchar(50) DEFAULT NULL,
  `qr_code` varchar(50) DEFAULT NULL,
  `logo` varchar(50) DEFAULT NULL,
  `picture` varchar(50) DEFAULT NULL,
  `sides` tinyint(1) DEFAULT NULL,
  `exclusions` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ds_coupons1` */

insert  into `ds_coupons1`(`id`,`item`,`discount`,`details`,`expiry`,`bar_code`,`qr_code`,`logo`,`picture`,`sides`,`exclusions`) values (1,'Denim Shirts','48','kjjsd bkjda ckasd kjads jkdsjj hckads jkjdas 													','0000-00-00',NULL,NULL,'Logo_50px.png','2.jpg',1,NULL),(2,'Denim Shirts','50','vjkjhh jnjjkvera jnakrs jvakdnn nj avcn jalkd jjajnd kkjlka kjlkad kjkarnn jafj jhjh','2016-08-15',NULL,NULL,'3.jpg','2.jpg',2,'These are Exclusions'),(3,'Denim TShirts','35','vjkjhh jnjjkvera jnakrs jvakdnn nj avcn jalkd jjajnd kkjlka kjlkad kjkarnn jafj jhjh','2016-08-15',NULL,NULL,'3.jpg','2.jpg',2,'These are Exclusions'),(4,'Lap Tops','50','nlkfec onsd kvldzf kjjlksdn lkncz hhknca nlkcsd nlacs','2016-03-15',NULL,NULL,'','london_exhibition2_255x229.jpg',2,'bjkh hkjsd jhlkn hlkn llkn');

/*Table structure for table `ds_retailerinfo` */

DROP TABLE IF EXISTS `ds_retailerinfo`;

CREATE TABLE `ds_retailerinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_name` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `gps_coordinate` varchar(1000) DEFAULT NULL,
  `logo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `ds_retailerinfo` */

insert  into `ds_retailerinfo`(`id`,`retailer_name`,`address`,`gps_coordinate`,`logo`) values (1,'test retailer','cjacksvkbvdkfbvd45','',''),(2,'abcd store','G-15, Islamabad, Pakistan','',''),(3,'testing','teskbnnvs',NULL,NULL);

/*Table structure for table `ds_reward` */

DROP TABLE IF EXISTS `ds_reward`;

CREATE TABLE `ds_reward` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `pic_ref` varchar(500) DEFAULT NULL,
  `points` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ds_reward` */

insert  into `ds_reward`(`id`,`name`,`type_id`,`description`,`pic_ref`,`points`,`stock`,`status`) values (1,'Test Reward',2,'This is test for editing',NULL,3000,10,0),(2,'New Reward',1,'This is for test purpose only',NULL,500,15,0),(4,'Test Reward',3,'This is for test editing',NULL,3000,10,1);

/*Table structure for table `ds_reward_types` */

DROP TABLE IF EXISTS `ds_reward_types`;

CREATE TABLE `ds_reward_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ds_reward_types` */

/*Table structure for table `group` */

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Specifies whether this permission is a default setting for new accounts.',
  `can_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Specifies whether this permission can be deleted from the control panel.',
  `theme` varchar(100) NOT NULL DEFAULT 'default' COMMENT 'The theme assigned to primary users in this group.',
  `landing_page` varchar(200) NOT NULL DEFAULT 'dashboard' COMMENT 'The page to take primary members to when they first log in.',
  `new_user_title` varchar(200) NOT NULL DEFAULT 'New User' COMMENT 'The default title to assign to new primary users.',
  `icon` varchar(100) NOT NULL DEFAULT 'fa fa-user' COMMENT 'The icon representing primary users in this group.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `group` */

insert  into `group`(`id`,`name`,`is_default`,`can_delete`,`theme`,`landing_page`,`new_user_title`,`icon`) values (1,'User',2,0,'default','dashboard','New User','fa fa-user'),(2,'Administrator',0,0,'nyx','dashboard','Brood Spawn','fa fa-flag'),(3,'Zerglings',0,1,'nyx','dashboard','Tank Fodder','sc sc-zergling'),(4,'consumer',0,1,'default','consumer','New User','fa fa-user'),(5,'reward',0,0,'default','reward','New User','fa fa-user'),(6,'coupon',0,1,'default','coupon','New User','fa fa-user');

/*Table structure for table `group_user` */

DROP TABLE IF EXISTS `group_user`;

CREATE TABLE `group_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Maps users to their group(s)';

/*Data for the table `group_user` */

insert  into `group_user`(`id`,`user_id`,`group_id`) values (1,1,1),(2,2,4),(4,5,4),(5,3,4),(7,7,4),(10,10,4),(11,11,4),(12,12,5),(13,13,4),(14,14,4),(15,15,4),(16,16,4),(17,17,4),(18,18,4),(19,19,4),(20,20,4),(21,21,6);

/*Table structure for table `security_question` */

DROP TABLE IF EXISTS `security_question`;

CREATE TABLE `security_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `security_question` */

insert  into `security_question`(`id`,`question`) values (1,'question1'),(2,'question2'),(3,'question3'),(4,'question4');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `locale` varchar(10) NOT NULL DEFAULT 'en_US' COMMENT 'The language and locale to use for this user.',
  `primary_group_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'The id of this user''s primary group.',
  `secret_token` varchar(32) NOT NULL DEFAULT '' COMMENT 'The current one-time use token for various user activities confirmed via email.',
  `flag_verified` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Set to ''1'' if the user has verified their account via email, ''0'' otherwise.',
  `flag_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Set to ''1'' if the user''s account is currently enabled, ''0'' otherwise.  Disabled accounts cannot be logged in to, but they retain all of their data and settings.',
  `flag_password_reset` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Set to ''1'' if the user has an outstanding password reset request, ''0'' otherwise.',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`user_name`,`display_name`,`email`,`title`,`locale`,`primary_group_id`,`secret_token`,`flag_verified`,`flag_enabled`,`flag_password_reset`,`created_at`,`updated_at`,`password`) values (1,'ranaosama','Rana Osama','r_m_o135@hotmail.com','New User','en_US',1,'',1,1,0,'2015-12-30 01:46:01','2015-12-30 01:46:01','$2y$10$EcMBEGY0881HH3G8lLKJiuGDYtmEQzsg2mA2.MDe9Qqeill/IAAxK'),(2,'osama','Rana Osama','r_m_o135@yahoo.com','New User','en_US',4,'3408c48c684248c5f2ad4a6238dde7c4',1,1,0,'2015-12-30 01:50:49','2015-12-30 01:50:49','$2y$10$EcMBEGY0881HH3G8lLKJiuGDYtmEQzsg2mA2.MDe9Qqeill/IAAxK'),(3,'r_m_o135@hotmail.co','Osama Rana','r_m_o135@hotmail.co','End User','en_US',4,'',1,1,1,'2015-12-30 00:00:00','0000-00-00 00:00:00',''),(4,'1@2.com','kashan','1@2.com','New User','en_US',4,'f3fe4308bea1923bd1553cf8d1dc1f93',1,1,1,'2015-12-31 06:40:18','2015-12-31 06:40:18','$2y$10$EcMBEGY0881HH3G8lLKJiuGDYtmEQzsg2mA2.MDe9Qqeill/IAAxK'),(7,'sami_jan@hotmail.com','Sami ullah','sami_jan@hotmail.com','End User','en_US',4,'c176ff9810b0bade0018035cdac3fe86',1,1,1,'2016-01-01 05:17:36','2016-01-03 11:57:10',''),(10,'hamood@dealsmash.com','Hamood Riasat Ali','hamood@dealsmash.com','End User','en_US',4,'714f73d886eaab493104e78fef2624cb',1,1,1,'2016-01-01 08:58:00','0000-00-00 00:00:00',''),(11,'test@mail.to','Ali Husnain','test@mail.to','End User','en_US',4,'0ae5d1e5350611c27f9a959d427f5d02',1,1,1,'2016-01-04 00:40:27','0000-00-00 00:00:00',''),(12,'rana','Rana Osama','rmo135.kiet@gmail.com','New User','en_US',5,'d16f4dbade25822f73d97f1e2cd8aabc',1,1,1,'2016-01-04 06:03:51','2016-01-04 06:03:51','$2y$10$EcMBEGY0881HH3G8lLKJiuGDYtmEQzsg2mA2.MDe9Qqeill/IAAxK'),(13,'r_m_o135@yahoo.co','Ali Husnain Shah','r_m_o135@yahoo.co','End User','en_US',4,'31cb26b3bfc8a37b2e68eb599350e0a3',1,1,1,'2016-01-04 07:15:44','0000-00-00 00:00:00',''),(20,'osama.rana@dealsmash.co','Test For Question','osama.rana@dealsmash.co','End User','en_US',4,'8116737d0c836c78a703c82e33d9723a',1,1,1,'2016-01-05 23:38:44','0000-00-00 00:00:00',''),(21,'osama.coupon','Coupon Creator','osama.coupon@cop.co','New User','en_US',6,'963b7e105cd687979b339eb0cae91861',1,1,1,'2016-01-09 08:32:16','2016-01-09 08:32:16','$2y$10$EcMBEGY0881HH3G8lLKJiuGDYtmEQzsg2mA2.MDe9Qqeill/IAAxK');

/*Table structure for table `user_event` */

DROP TABLE IF EXISTS `user_event`;

CREATE TABLE `user_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `event_type` varchar(255) NOT NULL COMMENT 'An identifier used to track the type of event.',
  `occurred_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

/*Data for the table `user_event` */

insert  into `user_event`(`id`,`user_id`,`event_type`,`occurred_at`,`description`) values (1,1,'sign_up','2015-12-30 11:46:01','User ranaosama successfully registered on 2015-12-30 01:46:01.'),(2,1,'sign_in','2015-12-30 11:48:01','User ranaosama signed in at 2015-12-30 01:48:01.'),(3,2,'sign_up','2015-12-30 11:50:49','User osama was created by ranaosama on 2015-12-30 01:50:49.'),(4,2,'password_reset_request','2015-12-30 11:50:49','User osama requested a password reset on 2015-12-30 01:50:49.'),(5,2,'sign_in','2015-12-30 11:53:11','User osama signed in at 2015-12-30 01:53:11.'),(7,2,'sign_in','2015-12-30 16:21:25','User osama signed in at 2015-12-30 06:21:25.'),(8,1,'sign_in','2015-12-31 09:53:11','User ranaosama signed in at 2015-12-30 23:53:11.'),(9,1,'sign_in','2015-12-31 16:00:10','User ranaosama signed in at 2015-12-31 06:00:10.'),(10,1,'sign_in','2015-12-31 16:39:39','User ranaosama signed in at 2015-12-31 06:39:39.'),(11,5,'sign_up','2015-12-31 16:40:18','User 1@2.com was created by ranaosama on 2015-12-31 06:40:18.'),(12,5,'password_reset_request','2015-12-31 16:40:18','User 1@2.com requested a password reset on 2015-12-31 06:40:18.'),(13,2,'sign_in','2016-01-01 11:22:43','User osama signed in at 2016-01-01 01:22:43.'),(16,7,'sign_up','2016-01-01 15:17:37','User sami_jan@hotmail.com was created by osama on 2016-01-01 05:17:36.'),(17,7,'password_reset_request','2016-01-01 15:17:37','User sami_jan@hotmail.com requested a password reset on 2016-01-01 05:17:36.'),(18,8,'sign_up','2016-01-01 17:24:38','User ali_husnain@hotmail.com was created by osama on 2016-01-01 07:24:38.'),(19,8,'password_reset_request','2016-01-01 17:24:38','User ali_husnain@hotmail.com requested a password reset on 2016-01-01 07:24:38.'),(20,9,'sign_up','2016-01-01 17:25:30','User ali_husnain@hotmail.com was created by osama on 2016-01-01 07:25:30.'),(21,9,'password_reset_request','2016-01-01 17:25:30','User ali_husnain@hotmail.com requested a password reset on 2016-01-01 07:25:30.'),(22,2,'sign_in','2016-01-01 18:49:14','User osama signed in at 2016-01-01 08:49:14.'),(23,2,'sign_in','2016-01-01 18:50:16','User osama signed in at 2016-01-01 08:50:16.'),(24,10,'sign_up','2016-01-01 18:58:01','User hamood@dealsmash.com was created by osama on 2016-01-01 08:58:00.'),(25,10,'password_reset_request','2016-01-01 18:58:01','User hamood@dealsmash.com requested a password reset on 2016-01-01 08:58:00.'),(26,1,'sign_in','2016-01-03 21:47:21','User ranaosama signed in at 2016-01-03 11:47:21.'),(27,2,'sign_in','2016-01-03 21:47:45','User osama signed in at 2016-01-03 11:47:45.'),(28,2,'sign_in','2016-01-03 21:48:41','User osama signed in at 2016-01-03 11:48:41.'),(29,11,'sign_up','2016-01-04 10:40:28','User test@mail.to was created by osama on 2016-01-04 00:40:27.'),(30,11,'password_reset_request','2016-01-04 10:40:28','User test@mail.to requested a password reset on 2016-01-04 00:40:27.'),(31,2,'sign_in','2016-01-04 11:08:10','User osama signed in at 2016-01-04 01:08:10.'),(32,2,'sign_in','2016-01-04 11:13:52','User osama signed in at 2016-01-04 01:13:52.'),(33,1,'sign_in','2016-01-04 16:02:48','User ranaosama signed in at 2016-01-04 06:02:48.'),(34,12,'sign_up','2016-01-04 16:03:51','User rana was created by ranaosama on 2016-01-04 06:03:51.'),(35,12,'password_reset_request','2016-01-04 16:03:51','User rana requested a password reset on 2016-01-04 06:03:51.'),(36,12,'sign_in','2016-01-04 16:05:28','User rana signed in at 2016-01-04 06:05:28.'),(37,13,'sign_up','2016-01-04 17:15:45','User r_m_o135@yahoo.co was created by rana on 2016-01-04 07:15:44.'),(38,13,'password_reset_request','2016-01-04 17:15:45','User r_m_o135@yahoo.co requested a password reset on 2016-01-04 07:15:44.'),(39,12,'sign_in','2016-01-04 17:25:45','User rana signed in at 2016-01-04 07:25:45.'),(40,12,'sign_in','2016-01-04 17:53:52','User rana signed in at 2016-01-04 07:53:52.'),(41,2,'sign_in','2016-01-04 18:03:35','User osama signed in at 2016-01-04 08:03:35.'),(42,12,'sign_in','2016-01-04 19:07:25','User rana signed in at 2016-01-04 09:07:25.'),(43,12,'sign_in','2016-01-04 19:13:41','User rana signed in at 2016-01-04 09:13:41.'),(44,2,'sign_in','2016-01-05 12:43:31','User osama signed in at 2016-01-05 02:43:31.'),(45,14,'sign_up','2016-01-05 14:20:34','User osama.rana@dealsmash.co was created by osama on 2016-01-05 04:20:33.'),(46,14,'password_reset_request','2016-01-05 14:20:34','User osama.rana@dealsmash.co requested a password reset on 2016-01-05 04:20:33.'),(47,15,'sign_up','2016-01-05 14:20:55','User osama.rana@dealsmash.co was created by  on 2016-01-05 04:20:55.'),(48,15,'password_reset_request','2016-01-05 14:20:55','User osama.rana@dealsmash.co requested a password reset on 2016-01-05 04:20:55.'),(49,16,'sign_up','2016-01-05 14:23:18','User osama.rana@dealsmash.co was created by  on 2016-01-05 04:23:18.'),(50,16,'password_reset_request','2016-01-05 14:23:18','User osama.rana@dealsmash.co requested a password reset on 2016-01-05 04:23:18.'),(51,17,'sign_up','2016-01-05 14:23:26','User osama.rana@dealsmash.co was created by  on 2016-01-05 04:23:25.'),(52,17,'password_reset_request','2016-01-05 14:23:26','User osama.rana@dealsmash.co requested a password reset on 2016-01-05 04:23:25.'),(53,2,'sign_in','2016-01-05 14:23:59','User osama signed in at 2016-01-05 04:23:59.'),(54,18,'sign_up','2016-01-05 14:24:57','User osama.rana@dealsmash.co was created by osama on 2016-01-05 04:24:57.'),(55,18,'password_reset_request','2016-01-05 14:24:57','User osama.rana@dealsmash.co requested a password reset on 2016-01-05 04:24:57.'),(56,19,'sign_up','2016-01-05 14:26:08','User osama.rana@dealsmash.co was created by osama on 2016-01-05 04:26:08.'),(57,19,'password_reset_request','2016-01-05 14:26:09','User osama.rana@dealsmash.co requested a password reset on 2016-01-05 04:26:08.'),(58,20,'sign_up','2016-01-06 09:38:45','User osama.rana@dealsmash.co was created by osama on 2016-01-05 23:38:44.'),(59,20,'password_reset_request','2016-01-06 09:38:45','User osama.rana@dealsmash.co requested a password reset on 2016-01-05 23:38:44.'),(60,12,'sign_in','2016-01-07 17:11:26','User rana signed in at 2016-01-07 07:11:26.'),(61,2,'sign_in','2016-01-07 17:12:04','User osama signed in at 2016-01-07 07:12:04.'),(62,1,'sign_in','2016-01-09 18:29:58','User ranaosama signed in at 2016-01-09 08:29:58.'),(63,21,'sign_up','2016-01-09 18:32:17','User osama.coupon was created by ranaosama on 2016-01-09 08:32:16.'),(64,21,'password_reset_request','2016-01-09 18:32:17','User osama.coupon requested a password reset on 2016-01-09 08:32:16.'),(65,21,'sign_in','2016-01-09 18:35:40','User osama.coupon signed in at 2016-01-09 08:35:40.'),(66,21,'sign_in','2016-01-09 20:04:22','User osama.coupon signed in at 2016-01-09 10:04:22.');

/*Table structure for table `user_rememberme` */

DROP TABLE IF EXISTS `user_rememberme`;

CREATE TABLE `user_rememberme` (
  `user_id` int(11) NOT NULL,
  `token` varchar(40) NOT NULL,
  `persistent_token` varchar(40) NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_rememberme` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
